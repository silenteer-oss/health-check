#!/bin/bash

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
cd "${CURRENT_DIR}"
rm -rf "${CURRENT_DIR}/health-check" || true

#build go
GOOS=linux GO111MODULE=on CGO_ENABLED=0  go build -ldflags '-w' health-check.go
upx "${CURRENT_DIR}/health-check"

VERSION=v1.4

HOST_JAVA="registry.gitlab.com/silenteer-oss/health-check/java"
docker build -f ${CURRENT_DIR}/Dockerfile_Java  -t ${HOST_JAVA}:${VERSION} .
docker push ${HOST_JAVA}:${VERSION}

HOST_GO="registry.gitlab.com/silenteer-oss/health-check/go"
docker build -f ${CURRENT_DIR}/Dockerfile_Go  -t ${HOST_GO}:${VERSION} .
docker push ${HOST_GO}:${VERSION}

rm -rf "${CURRENT_DIR}/health-check" || true
