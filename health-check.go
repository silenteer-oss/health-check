package main

import (
	_ "emperror.dev/errors"
	"encoding/json"
	"fmt"
	"github.com/nats-io/nats.go"
	"github.com/sebest/logrusly"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.com/silenteer-oss/titan"
	"logur.dev/logur"
	"os"
	"strings"
	"time"
)

var hostname string

var logger logur.Logger

const (
	NatsServers     = "Nats.Servers"
	NatsReadTimeout = "Nats.ReadTimeout"
)

const logglyToken = "62b001a1-04d4-4d4e-9449-b74fa11c5cf9"

func init() {
	var err error
	hostname, err = os.Hostname()
	if hostname == "" || err != nil {
		hostname = "localhost"
	}

	viper.AddConfigPath(".")
	viper.SetConfigName("config")

	viper.AutomaticEnv() // read in environment variables that match
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}

	// nats
	viper.SetDefault(NatsServers, "nats://127.0.0.1:4222, nats://localhost:4222")
	viper.SetDefault(NatsReadTimeout, 99999)
}

func main() {
	os.Exit(doCheck())
}

func doCheck() int {
	var err error

	logger := logrus.New()
	hook := logrusly.NewLogglyHook(logglyToken, "https://logs-01.loggly.com/bulk/", logrus.DebugLevel, "go", "logrus")
	logger.Hooks.Add(hook)

	hook.Flush()

	logger.Info("host name " + hostname)
	logger.Info("Nats server " + viper.GetString(NatsServers))

	conn, err := NewConnection()
	if err != nil {
		logger.Error(fmt.Sprintf("Cannot connect to NATS server %+v ", err))
		return 1
	}
	logger.Info("connect to nats server successfully ")

	healthCheckCallBack := make(chan interface{}, 1000)
	sub, err := conn.Conn.QueueSubscribe(titan.HEALTH_CHECK_REPLY, "", func(p *titan.Message) error {
		var health titan.Health
		err := json.Unmarshal(p.Body, &health)
		if err != nil {
			logger.Error(fmt.Sprintf("desrialize body to Health object error: %+v", err))
			return nil
		}
		logger.Info("received health check ", string(p.Body))
		if health.HostName == hostname {
			healthCheckCallBack <- "received callback"
		}
		return nil
	})
	if err != nil {
		logger.Error(fmt.Sprintf("subscribe health check error %+v ", err))
		return 1
	}

	defer sub.Drain()
	defer conn.Conn.Drain()

	//2. test publish
	type TestBody struct {
		Msg string `json:"msg"`
	}

	logger.Info("Sending test message...")
	healthCheckSubject := fmt.Sprintf("%s_%s", titan.HEALTH_CHECK, strings.ReplaceAll(hostname, " ", "_"))
	err = conn.Conn.Publish(healthCheckSubject, TestBody{Msg: "test msg"})
	if err != nil {
		logger.Error(fmt.Sprintf("error on sending health check command %+v ", err))
		return 1
	}

	// wait for all messages processed or timeout
	result := 0
	select {
	case <-healthCheckCallBack:
		logger.Info("received health check response")
	case <-time.After(15 * time.Second):
		logger.Error("time out ")
		result = 1
	}
	logger.Info("App exit code ", result)
	return result
}

func NewConnection() (*titan.Connection, error) {
	return titan.NewConnection(
		viper.GetString(NatsServers),
		nats.Timeout(10*time.Second), // connection timeout
		nats.Name(fmt.Sprintf("%s_%s", hostname, titan.HEALTH_CHECK)),
		nats.MaxReconnects(-1), // never give up
		nats.ErrorHandler(func(_ *nats.Conn, _ *nats.Subscription, e error) {
			if e != nil {
				logger.Error(fmt.Sprintf("Nats server error %+v", e))
			}
		}),
		nats.DisconnectErrHandler(func(_ *nats.Conn, e error) {
			if e != nil {
				logger.Error(fmt.Sprintf("Nats server disconect error %+v", e))
			}
		}),
		nats.ReconnectHandler(func(_ *nats.Conn) {
			logger.Debug("Nats server  Reconnect")
		}),
		nats.DiscoveredServersHandler(func(_ *nats.Conn) {
			logger.Debug("Nats server  Discovered")
		}),
	)
}
