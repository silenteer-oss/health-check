module gitlab.com/silenteer-oss/health-check

go 1.14

require (
	emperror.dev/errors v0.8.0
	github.com/nats-io/nats.go v1.9.2
	github.com/sebest/logrusly v0.0.0-20180315190218-3235eccb8edc
	github.com/segmentio/go-loggly v0.5.0 // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/viper v1.6.2
	gitlab.com/silenteer-oss/titan v1.0.44
	logur.dev/logur v0.16.2
)
